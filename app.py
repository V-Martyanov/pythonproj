from flask import Flask, render_template, url_for, request, redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///blog.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class Orders(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    intro = db.Column(db.String(200), nullable=False)
    # Огромный объем текста
    text = db.Column(db.Text, nullable=False)
    date = db.Column(db.DateTime, default=datetime.utcnow)
    fname = db.Column(db.String(100), nullable=False)
    sname = db.Column(db.String(200), nullable=False)
    number = db.Column(db.Text, nullable=False)
    # Объект и его айди
    def __repr__(self):
        return '<Orders %r>' % self.id 


class Article(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    intro = db.Column(db.String(200), nullable=False)
    # Огромный объем текста
    text = db.Column(db.Text, nullable=False)
    fname = db.Column(db.String(100), nullable=False)
    sname = db.Column(db.String(200), nullable=False)
    number = db.Column(db.Text, nullable=False)
    date = db.Column(db.DateTime, default=datetime.utcnow)
    # Объект и его айди
    def __repr__(self):
        return '<Article %r>' % self.id 


class Prod(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    intro = db.Column(db.String(200), nullable=False)
    # Огромный объем текста
    text = db.Column(db.Text, nullable=False)
    date = db.Column(db.DateTime, default=datetime.utcnow)
    # user_id = db.Column(db.Integer, primary_key=True)
    # Объект и его айди
    def __repr__(self):
        return '<Prod %r>' % self.id 


# @ - юрел адрес
# / - главная страница
@app.route('/')
@app.route('/home')
def hello_world():
    product = Prod.query.order_by(Prod.date.desc()).all()
    return render_template("index.html", product=product)


@app.route('/orders')
def orders():
    order = Orders.query.order_by(Orders.date.desc()).all()
    return render_template("orders.html", order=order)

@app.route('/order/<int:id>')
def order_delete(id):
    order = Orders.query.get_or_404(id)

    try:
        db.session.delete(order)
        db.session.commit()
        return redirect('/')
    except:
        return "При удалении статьи произошла ошибка"

@app.route('/about')
def about():
    product = Prod.query.order_by(Prod.date.desc()).all()
    return render_template("about.html", product=product)


@app.route('/about/<int:id>')
def about_detail(id):
    product = Prod.query.get(id)
    return render_template("about_details.html", product=product)


@app.route('/about/<int:id>/development')
def development(id):
    product = Prod.query.get_or_404(id)

    try:
        db.session.delete(product)
        db.session.commit()
        return redirect('/about')
    except:
        return "При удалении продукта произошла ошибка"


@app.route('/posts')
def posts():
    articles = Article.query.order_by(Article.date.desc()).all()
    return render_template("posts.html", articles=articles)


@app.route('/posts/<int:id>')
def posts_detail(id):
    article = Article.query.get(id)
    return render_template("posts_detail.html", article=article)


@app.route('/posts/<int:id>/del')
def posts_delete(id):
    article = Article.query.get_or_404(id)

    try:
        db.session.delete(article)
        db.session.commit()
        return redirect('/posts')
    except:
        return "При удалении статьи произошла ошибка"


@app.route('/posts/<int:id>/update', methods = ['POST', 'GET'])
def posts_update(id):
    article = Article.query.get(id)
    if request.method == 'POST':
        article.fname = request.form['fname']
        article.sname = request.form['sname']
        article.number = request.form['number']

        try:
            db.session.commit()
            return redirect('/posts')
        except:
            return "При редакции статьи произошла ошибка"
    else:
        return render_template("post_update.html", article=article)


@app.route('/posts/<int:id>/orders', methods = ['POST', 'GET'])
def posts_orders(id):
    article = Article.query.get(id)

    fname = article.fname
    sname = article.sname
    number = article.number
    title = article.title
    intro = article.intro
    text = article.text

    order = Orders(fname=fname, sname=sname, number=number, title=title, intro=intro, text=text)

    try:
        db.session.add(order)
        db.session.commit()
        return redirect('/orders')
    except:
        return "При добавлении заказа произошла ошибка"


@app.route('/user/<string:Name>/<int:id>')
def User(Name, id):
    return "User : " + Name +" id : " + str(id)


@app.route('/create-article/<int:id>', methods = ['POST', 'GET'])
def create_article(id):
    product = Prod.query.get(id)
    if request.method == 'POST':  
        fname = request.form['fname']
        sname = request.form['sname']
        number = request.form['number']
        title = product.title
        intro = product.intro
        text = product.text

        article = Article(fname=fname, sname=sname, number=number, title=title, intro=intro, text=text)

        try:
            db.session.add(article)
            db.session.commit()
            return redirect('/posts')
        except:
            return "При добавлении статьи произошла ошибка"
    else:
        return render_template("create-article.html")


@app.route('/add', methods = ['POST', 'GET'])
def add_proj():
    if request.method == 'POST':
        title = request.form['title']
        intro = request.form['intro']
        text = request.form['text']

        product = Prod(title=title, intro=intro, text=text)

        try:
            db.session.add(product)
            db.session.commit()
            return redirect('/about')
        except:
            return "При добавлении статьи произошла ошибка"
    else:
        return render_template("add-prod.html")

if __name__ == "__main__":
    app.run(debug=True)

# deactivate
# source venv/bin/activate
#<button class = "btn btn-warning">Детальней</button>