Для запуска с локальной машины необходимо скачать виртуальную среду venv, она нужна для работы в flask.
1. sudo apt install python3-venv

В вашей папке, где находиться проект

2. python3 -m venv venv

Входит в вирт. среду

3. source venv/bin/activate

Ctrl+c - для завершения работы сайта

4. python app.py 

Выходит из виртуальной среды

5. deactivate
